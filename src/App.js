import React from "react";

const App = () => {
  return (
    <div className="row d-flex">
      {/* Partie profil */}
      <div className="photo col-12 col-md-4 p-1 bg-warning">
        <section className="m-3 text-justify row">
          <div className=" col-12 col-md-2"></div>
          <div className="col-12 col-md-10 ">
            <section className="p-1">
              <img
                className="rounded-circle img-fluid"
                src="./Images/user-avatar.jpg"
                alt="monimg"
              />
              <h3 className="text-light text-center">Noel Robinson Ndecky</h3>
              <h4 className="text-light text-center">Bakéliste</h4>
              <h4 className="text-light text-center">Développeur web</h4>
            </section>
          </div>
        </section>
        <section className="mobile m-3 text-justify row">
          <div className="col-12 col-md-2">
            <i className="fa fa-solid fa-phone logo1"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="p-1">
              <h5 className="text-light">+221 78 245 56 78</h5>
              <p className="text-light">Mobile</p>
            </section>
            <hr className="border-light"></hr>
          </div>
        </section>
        <section className="personnal m-3 text-justify row">
          <div className="col-12 col-md-2">
            <i className="fa-solid fa-envelope logo1"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="p-1">
              <h5 className="text-light">azerty@gmail.com</h5>
              <p className="text-light">Personnal</p>
            </section>
            <hr className="border-light"></hr>
          </div>
        </section>
        <section className="home m-3 text-justify row">
          <div className="col-12 col-md-2">
            <i className="fa-solid fa-location-dot logo1"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="p-1">
              <h5 className="text-light">
                H-123 Block A <br /> State Country
              </h5>
              <p className="text-light">Home</p>
            </section>
            <hr className="border-light"></hr>
          </div>
        </section>
        <section className="t-skills m-3 text-justify row">
          <div className="col-12 col-md-2">
            <i className="fa-solid fa-clipboard-list logo1"></i>
          </div>
          <div className="col-12 col-md-10 pt-4">
            <section className="p-1">
              <h6 className="text-light">
                <strong>Technical Skills</strong>
              </h6>
              <div className="row">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>HTML5, Css3, Jquery</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar html">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Gitlab</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar gitlab">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Bootstrap</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar bootstrap">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Wordpress</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar wordpress">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Javascript</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar javascript">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Photoshop, Ilustrator, Xd...</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar photoshop">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Reactjs</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar react">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Freecad</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar freecad">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row pt-4">
                <div className="col-12 col-md-3">
                  <div className="text-center"></div>
                </div>
                <div className="col-12 col-md-9">
                  <div>
                    <p className="text-light">
                      <strong>Arduino</strong>
                    </p>
                    <div className="progress">
                      <div className="progress-bar arduino">
                        <span className="circle1"></span>
                      </div>
                    </div>
                  </div>
                  <hr className="border-light" />
                </div>
              </div>
            </section>
          </div>
        </section>
        <section className="p-skills m-3 text-justify row">
          <div className="col-12 col-md-2">
            <i className="fa-solid fa-clipboard-list logo1"></i>
          </div>
          <div className="col-12 col-md-10 ">
            <section className="p-1">
              <h6 className="text-light pt-3">Personnal Skills</h6>
              <div>
                <div className="row pt-4">
                  <div className="col-12 col-md-3">
                    <div className="text-center"></div>
                  </div>
                  <div className="col-12 col-md-9">
                    <div>
                      <p className="text-light">
                        <strong>Creativité</strong>
                      </p>
                      <div className="progress">
                        <div className="progress-bar creativite">
                          <span className="circle1"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row pt-4">
                  <div className="col-12 col-md-3">
                    <div className="text-center"></div>
                  </div>
                  <div className="col-12 col-md-9">
                    <div>
                      <p className="text-light">
                        <strong>Leadership</strong>
                      </p>
                      <div className="progress">
                        <div className="progress-bar lead">
                          <span className="circle1"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row pt-4 mb-5">
                  <div className="col-12 col-md-3">
                    <div className="text-center"></div>
                  </div>
                  <div className="col-12 col-md-9">
                    <div>
                      <p className="text-light">
                        <strong>Communication</strong>
                      </p>
                      <div className="progress">
                        <div className="progress-bar com">
                          <span className="circle1"></span>
                        </div>
                      </div>
                    </div>
                    <hr className="border-light" />
                  </div>
                </div>
              </div>
            </section>
          </div>
        </section>
      </div>
      {/* Partie info */}
      <div className="info card col-12 col-md-8 p-1">
        <section className="About m-3 p-2 text-justify row border">
          <div className="col-12 col-md-2 mb-3 text-center">
            <i className="fa-solid fa-user logo2"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="card p-3">
              <h2 className="text-warning">A propos</h2>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam
                voluptatibus molestias, quasi quaerat minus libero saepe sed,
                cupiditate doloremque, molestiae odit tempora harum iste amet ea
                hic reprehenderit ipsam incidunt?
              </p>
            </section>
          </div>
        </section>
        <section className="Experience m-3 p-2 text-justify row border">
          <div className="col-12 col-md-2 mb-3 text-center">
            <i className="fa-solid fa-briefcase logo2"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="card p-3">
              <h2 className="text-warning">Work Experience</h2>
              <div className="work mb-3">
                <h6>Designation at compagny name</h6>
                <span>Apr, 2015 Nov 2017</span>
                <p>
                  Competently parallel task interoperable ideas without
                  open-source core competencies. Distinctively productivate
                  vertical convergence.
                </p>
              </div>
              <div className="work mb-3">
                <h6>Designation at compagny name</h6>
                <span>Apr, 2015 Nov 2017</span>
                <p>
                  Competently parallel task interoperable ideas without
                  open-source core competencies. Distinctively productivate
                  vertical convergence.
                </p>
              </div>
              <div className="mb-3">
                <h6>Designation at compagny name</h6>
                <span>Apr, 2015 Nov 2017</span>
                <p>
                  Competently parallel task interoperable ideas without
                  open-source core competencies. Distinctively productivate
                  vertical convergence.
                </p>
              </div>
            </section>
          </div>
        </section>
        <section className="Education m-3 p-2 text-justify row border">
          <div className="col-12 col-md-2 mb-3 text-center">
            <i className="fa-solid fa-graduation-cap logo2"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="card p-3">
              <h2 className="text-warning">Education</h2>
              <div className="edu mb-3">
                <h6>Diploma/Degree from college/Institute</h6>
                <span>Feb, 2004 Oct 2006</span>
                <p>
                  Competently parallel task interoperable ideas without
                  open-source core competencies.
                </p>
              </div>
              <div className="edu mb-3">
                <h6>Diploma/Degree from college/Institute</h6>
                <span>Feb, 2004 Oct 2006</span>
                <p>
                  Competently parallel task interoperable ideas without
                  open-source core competencies.
                </p>
              </div>
              <div className="mb-3">
                <h6>Diploma/Degree from college/Institute</h6>
                <span>Feb, 2004 Oct 2006</span>
                <p>
                  Competently parallel task interoperable ideas without
                  open-source core competencies.
                </p>
              </div>
            </section>
          </div>
        </section>
        <section className="Achievements m-3 p-2 text-justify row border">
          <div className="col-12 col-md-2 mb-3 text-center">
            <i className="fa-solid fa-award logo2"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="card p-3">
              <h2 className="text-warning">Achievements</h2>
              <div className="prime mb-3">
                <h6>Awards name at place</h6>
                <span>Feb 2014</span>
                <p>
                  Competently parallel task interoperable ideas without.
                  Competently parallel task interoperable ideas without.
                </p>
              </div>
              <div className="mb-3">
                <h6>Awards name at place</h6>
                <span>Feb 2014</span>
                <p>
                  Competently parallel task interoperable ideas without.
                  Competently parallel task interoperable ideas without.
                </p>
              </div>
            </section>
          </div>
        </section>
        <section className="Hobbies m-3 p-2 text-justify row border">
          <div className="col-12 col-md-2 mb-3 text-center">
            <i className="fa fa-smile-o logo2" aria-hidden="true"></i>
          </div>
          <div className="col-12 col-md-10">
            <section className="card p-3">
              <h2 className="text-warning">Hobbies</h2>
              <div className="d-flex">
                <div className="m-3">
                  <i className="fa fa-book logo3" aria-hidden="true"></i>
                </div>
                <div className="m-3">
                  <i className="fa fa-bicycle logo3" aria-hidden="true"></i>
                </div>
                <div className="m-3">
                  <i className="fa fa-code logo3" aria-hidden="true"></i>
                </div>
              </div>
            </section>
          </div>
        </section>
        <section className="mt-5">
          <div className="d-inline">
            <div className="card p-1">
              <p className="text-warning">
                <strong>Be</strong>/viewname <strong> In</strong>/viewname
              </p>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
};

export default App;
